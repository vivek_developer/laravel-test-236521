<?php

namespace App\Http\Controllers;

use DB;

class OrdersController extends Controller
{

    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData($id)
    {

        $order = DB::table('orders as os')->where('os.orderNumber', $id)->select('os.orderNumber', 'os.orderDate', 'os.status', 'os.customerNumber')->first();
        if (!$order) {
            return response()->json('order not found', 400);
        }
        $response               = [];
        $response['order_id']   = $order->orderNumber;
        $response['order_date'] = $order->orderDate;
        $response['status']     = $order->status;

        $response['order_details'] = DB::table('orderdetails as od')->select('ps.productName as product', 'ps.productLine as product_line', 'od.quantityOrdered as qty', 'od.priceEach as unit_price', DB::raw('od.priceEach*od.quantityOrdered as line_total'))->where('orderNumber', $order->orderNumber)->leftjoin('products as ps', 'od.productCode', '=', 'ps.productCode')->get();
        $bill_total                = 0;
        foreach ($response['order_details'] as $row) {
            $bill_total += $row->line_total;
            $row->unit_price = (float) $row->unit_price;
        }
        $response['bill_amount'] = number_format($bill_total, 2, '.', '');
        $response['customer']    = DB::table('customers')->select('contactFirstName as first_name', 'contactLastName as last_name', 'phone', 'country as country_code')->where('customerNumber', $order->customerNumber)->first();
        return response()->json($response, 200);

    }
}
